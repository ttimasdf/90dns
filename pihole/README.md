SSH into your pihole machine and enter the following commands:

```bash
sudo sh -c 'echo ".*nintendo.*" >> /etc/pihole/regex.list'
pihole -w conntest.nintendowifi.net ctest.cdn.nintendo.net
sudo systemctl restart pihole-FTL
```

**Be aware that this will use Nintendo's conntest and ctest still, not our own!** As far as we know, there's no telemetry sent through these endpoints currently, however you should still keep this in mind. If you have any recommendations on how we can implement hijacking the DNS calls to replace with our own, please open an issue about it.

Thank you @duckyb for your help with this section.
