#!/usr/bin/python3
import sys
try:
    import requests
except ModuleNotFoundError:
    print("You need requests to use this script.")
    print("Run 'pip install requests' to install it.")
    sys.exit()
try:
    from dns import resolver  # dnspython
    import dns  # dnspython
except ModuleNotFoundError:
    print("You need dnspython to use this script.")
    print("Run 'pip install dnspython' to install it.")
    sys.exit()

print("90DNS Status Info Script by aveao/AveSatanas, released under GPLv2.")

dns_servers = ['163.172.141.219', '207.246.121.77']
http_servers = ['95.216.149.205', '207.246.121.77']

# Initialize resolver, set up the DNS servers
dns_resolver = resolver.Resolver()

# Set timeout to 3 seconds
dns_resolver.lifetime = 3

# Define a basic dataset, with a list of the most important domains
# Yes, I know that this is ugly, but I wrote it on 8AM after a sleepless night
test_dataset = [['nintendo.net', ['127.0.0.1']],
                ['nintendo.com', ['127.0.0.1']],
                ['ctest.cdn.nintendo.net', ['207.246.121.77', '95.216.149.205']],
                ['conntest.nintendowifi.net',
                 ['207.246.121.77', '95.216.149.205']],
                ['90dns.test', ['207.246.121.77', '95.216.149.205']],
                ['bugyo.hac.dp1.eshop.nintendo.net', ['127.0.0.1']],
                ]


def compare_dns(domain_to_test, expected_ips):
    # Query a domain's A records, convert first result to string
    # and check if it's in the list of expected IPs
    try:
        return (str(dns_resolver.query(domain_to_test, 'A')[0]) in expected_ips)
    except (resolver.NoAnswer, resolver.NXDOMAIN, dns.exception.Timeout):
        # No answer or no domain, fail
        return ""


print("Starting tests now.\n")

test_successes = 0
for dns_server in dns_servers:
    dns_resolver.nameservers = [dns_server]
    failures = 0
    for test_data in test_dataset:
        if not (compare_dns(test_data[0], test_data[1])):
            failures += 1
            # fstrings are good and all, but I wanted to support py2
            print("Incorrect records detected on {}, with {}."
                  .format(test_data[0], dns_server))
        else:
            test_successes += 1
            print("All good on {}, with {}.".format(test_data[0], dns_server))
    if not failures:
        print("\nAll queries on {} had the expected result.".format(dns_server))
    else:
        print("\n{} results on {} were incorrect.".format(failures, dns_server))

# If all tests succeeded, notify user of that.
if test_successes == (len(test_dataset) * len(dns_servers)):
    print("90DNS' DNS section is working perfectly.")
else:
    print("90DNS' DNS section is currently having issues.")

print()

test_successes = 0
for http_server in http_servers:
    ctest_issues = False
    conntest_issues = False
    try:
        ctestres = requests.get(f"http://{http_server}",
                                headers={"Host": "ctest.cdn.nintendo.net"},
                                timeout=3)
    except requests.exceptions.ConnectTimeout:
        print(f"ctest for {http_server} timed out")
        ctest_issues = True

    try:
        conntres = requests.get(f"http://{http_server}",
                                headers={"Host": "conntest.nintendowifi.net"},
                                timeout=3)
    except requests.exceptions.ConnectTimeout:
        print(f"conntest for {http_server} timed out")
        conntest_issues = True

    if ctest_issues or conntest_issues:
        continue

    if ctestres.status_code != 200:
        print(f"ctest for {http_server} returned HTTP{ctestres.status_code}")
        ctest_issues = True

    if conntres.status_code != 200:
        print(f"conntest for {http_server} returned HTTP{conntres.status_code}")
        conntest_issues = True

    if "X-Organization" not in ctestres.headers:
        print(f"ctest for {http_server} lacks X-Organization")
        ctest_issues = True
    elif ctestres.headers["X-Organization"] != "Nintendo":
        print(f"ctest for {http_server} has incorrect X-Organization: "
              f"{ctestres.headers['X-Organization']}")
        ctest_issues = True

    if "X-Organization" not in conntres.headers:
        print(f"conntest for {http_server} lacks X-Organization")
        conntest_issues = True
    elif conntres.headers["X-Organization"] != "Nintendo":
        print(f"conntest for {http_server} has incorrect X-Organization: "
              f"{conntres.headers['X-Organization']}")
        conntest_issues = True

    if ctestres.text != "ok":
        print(f"ctest for {http_server} has incorrect text: {ctestres.text}")
        ctest_issues = True

    if ctest_issues:
        print(f"ctest on {http_server} is currently having issues.")
    else:
        print(f"ctest on {http_server} is working perfectly.")

    if conntest_issues:
        print(f"conntest on {http_server} is currently having issues.")
    else:
        print(f"conntest on {http_server} is working perfectly.")
